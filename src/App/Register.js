import Redity from 'redity'
import Session from '../Pages/Session/Model'
import Home from '../Pages/Home/Model'
import Users from '../Pages/Users/Model'
// Register your models
Redity.register('session', Session)
Redity.register('home', Home)
Redity.register('my_model_for_register', Users)

export default Redity
