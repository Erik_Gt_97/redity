import React from 'react'
import Proptypes from 'prop-types'
import { connect } from 'redity'
import Input from '@aventura_tech/advance-erp-web-components/lib/atoms/Input'
import Select from '@aventura_tech/advance-erp-web-components/lib/atoms/Select'

function Section2 ({ user, setUser, validation, setValidation }) {
  return <div>
    <div>
      <label>Name</label>
      <Input
        name='name' value={user.name}
        onChange={ev => {
          setUser({
            ...setUser(),
            name: ev.target.value
          })
          setValidation({
            ...setValidation(),
            name: false
          })
        }}
        validation={validation.name}
      />
    </div>
    <div>
      <label>lastname</label>
      <Input
        name='lastname' value={user.lastname}
        onChange={ev => {
          setUser({
            ...setUser(),
            lastname: ev.target.value
          })
          setValidation({
            ...setValidation(),
            lastname: false
          })
        }}
        validation={validation.lastname}
      />
    </div>
    <div>
      <label>Age</label>
      <Input
        name='age' value={user.age}
        onChange={ev => {
          setUser({
            ...setUser(),
            age: ev.target.value
          })
          setValidation({
            ...setValidation(),
            age: false
          })
        }}
        validation={validation.age}
      />
    </div>
    <div>
      <label>Type</label>
      <Select
        name='type_id'
        value={user.type}
        options={[
          { value: '0', label: 'admin' },
          { value: '1', label: 'client' }
        ]}
        onChange={value => {
          setUser({
            ...setUser(),
            type: value
          })
          setValidation({
            ...setValidation(),
            type_id: false
          })
        }}
        validation={validation.type_id}
        placeholder='Seleccionar ... '
      />

    </div>
  </div>
}

Section2.Proptypes = {
  user: Proptypes.object.isRequired
}

const mapStateToProps = (states, setStates) => ({
  user: {
    name: states.user.name,
    lastname: states.user.lastname,
    age: states.user.age,
    type: states.user.type
  },
  setUser: setStates.user,
  validation: {
    name: states.validation.name,
    lastname: states.validation.lastname,
    age: states.validation.age,
    type_id: states.validation.type_id
  },
  setValidation: setStates.validation
})

export default connect('my_model_for_register', mapStateToProps)(Section2)
