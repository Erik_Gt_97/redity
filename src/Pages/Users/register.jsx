import React from 'react'
import Proptypes from 'prop-types'
import { connect } from 'redity'
import Section1 from './section1'
import Section2 from './section2'

function Register ({ setUser, postUser, restart, disabled }) {
  const handleSubmit = ev => {
    ev.preventDefault()
    const form = ev.target
    const data = {
      username: form.username.value,
      password: form.password.value,
      name: form.name.value,
      lastname: form.lastname.value,
      age: form.age.value,
      type_id: form.type_id.value
    }
    postUser(data)
  }

  React.useEffect(() => {
    return () => {
      restart()
    }
  })

  return <form onSubmit={handleSubmit}>
    <Section1 />
    <Section2 />
    <button disabled={disabled} onClick={() => setUser.init()}>Cancel</button>
    <button disabled={disabled} type='Submit'>Create</button>
  </form>
}

Register.propTypes = {
  postUser: Proptypes.func.isRequired
}

const mapDispatchToProps = dispatch => ({
  postUser: dispatch.postUser,
  restart: dispatch.restart
})

const mapStateToProps = (states, setStates) => ({
  disabled: states.disabled,
  setUser: setStates.user
})

export default connect('my_model_for_register', mapStateToProps, mapDispatchToProps)(Register)
