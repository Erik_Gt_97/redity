import React from 'react'
import Proptypes from 'prop-types'
import { connect } from 'redity'
import Input from '@aventura_tech/advance-erp-web-components/lib/atoms/Input'

function Section1 ({ user, setUser, validation, setValidation }) {
  return <fiv>
    <div>
      <label>Username</label>
      <Input
        name='username' value={user.username}
        onChange={ev => {
          setUser({
            ...setUser(),
            username: ev.target.value
          })
          setValidation({ ...setValidation(), username: false })
        }}
        validation={validation.username}
      />

    </div>
    <div>
      <label>Password</label>
      <Input
        name='password' value={user.password} type='password'
        onChange={ev => {
          setUser({
            ...setUser(),
            password: ev.target.value
          })
          setValidation({ ...setValidation(), password: false })
        }}
        validation={validation.password}
      />

    </div>
  </div>
}

Section1.proptypes = {
  user: Proptypes.object.isRequired,
  setUser: Proptypes.func.isRequired,
  validation: Proptypes.object.isRequired
}

const mapStateToProps = (states, setStates) => ({
  user: {
    username: states.user.username,
    password: states.user.password
  },
  setUser: setStates.user,
  validation: {
    username: states.validation.username,
    password: states.validation.password
  },
  setValidation: setStates.validation
})

export default connect('my_model_for_register', mapStateToProps)(Section1)
