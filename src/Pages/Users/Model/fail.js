export default (catchErr, states, header) => {
  const { postUser } = header.actions

  if (catchErr.response) {
    states.disabled.init()
    return
  }

  if (postUser) {
    const validate = {}
    for (const err of catchErr.response.data.errors) {
      validate[err.field] = {
        message: err.message,
        type: 'danger'
      }
    }

    states.disabled.init()
    states.validation({ ...states.validation(), ...validate })
  }
}
