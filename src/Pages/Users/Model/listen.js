import axios from 'axios'
export default async (payload, states, header) => {
  const { postUser } = header.actions
  const { restart } = header.actions
  if (postUser) {
    const validate = {}
    const setValidate = (key, message) => {
      validate[key] = {
        type: 'danger',
        message
      }
    }
    if (payload.username === '') {
      setValidate('username', 'Se requiere un nombre de usuario')
    }
    if (payload.password === '') {
      setValidate('password', 'Se requiere de una contraseña')
    }
    if (payload.name === '') {
      setValidate('name', 'Se requiere un nombre')
    }
    if (payload.lastname === '') {
      setValidate('lastname', 'Se requiere un apellido')
    }
    if (payload.type === '') {
      setValidate('type', 'Se requiere un tipo')
    }

    if (payload.age === '') {
      setValidate('age', 'Se requiere una edad')
    }

    if (Object.keys(validate).length > 0) {
      states.validation({ ...states.validation, ...validate })
      return
    }

    states.disabled(true)
    /* const res = await axios.post('path_api', payload)
    console.log(res.data.message) */
    console.log(payload)

    states.disabled.init()

    states.validation.init()
  }
  if (restart) {
    states.validation.initUnseen()
    states.disbled.initUnseen()
    states.user.init()
  }
}
