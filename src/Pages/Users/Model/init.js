export default initial => {
  initial.states.user = {

    username: '',
    password: '',
    name: '',
    lastname: '',
    age: '',
    type: false
  }
  initial.states.validation = {
    username: false,
    password: false,
    name: false,
    lastname: false,
    age: false,
    type_id: false
  }

  initial.states.disabled = false

  initial.dispatchers.postUser = null
  initial.dispatchers.restart = null
}
